package org.lifer.serverside

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ServersideApplication

fun main(args: Array<String>) {
	runApplication<ServersideApplication>(*args)
}
